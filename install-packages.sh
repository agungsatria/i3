#!/bin/bash
#Purpose: install package to match in i3 config
#Version: 
#Created Date:  Sun 21 Nov 2021 07:25:19 PM WIB
#Modified Date: 
#Author: Agung Satria
# START #
echo "Updating repo"
sudo apt update -y
echo "Installing i3blocks"
sudo apt install i3blocks -y
echo "Installing some package"
sudo apt install compton flameshot i3lock-fancy feh nvim vokoscreen qalc calcurse rofi arandr yad xdotool xclip grabc figlet -y
echo "Installing Nerd Fonts"
# sudo cp -r ./fonts/* /home/agung/.local/share/fonts/
sudo cp -r ./fonts/* ~/.local/share/fonts/
sudo fc-cache -fv
echo "For the rest, install from dotfiles repo"
echo "https://gitlab.com/agstr/dotfiles.git"
echo "Done"
# END #
