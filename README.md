## Screenshots
![](screenshots/vim.jpg)
![](screenshots/i3gaps.jpg)
<!-- link/path to image -->

[READ INSTRUCTIONS IN "BASIC SETTINGS" FOLDER]
Config:
* st
* dmenu
* i3block scripts
* i3status
* neovim config
* Compton config
* Rofi theme (https://github.com/siduck/chadwm/tree/main/rofi)
* Nerd Font (Iosevka, JetBrain, and Hack)

<!-- ROADMAP -->
## Installed packages used in keybinds

- [x] st (compile)
- [x] dmenu (compile)
- [ ] Rofi
- [ ] Lockscreen
    - [ ] Betterlockscreen
    - [ ] i3lock-fancy
- [ ] Flameshot
- [ ] ncmpcmm
- [ ] ranger
- [ ] feh
- [ ] vokoscreen
- [ ] qalc
- [ ] calcurse
- [ ] Brave browser

<!--See the [open issues](https://github.com/othneildrew/Best-README-Template/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>
-->
