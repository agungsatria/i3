#!/bin/bash
#Purpose: add ppa repo, and install i3 gaps
#Version: 
#Created Date:  Sun 21 Nov 2021 07:25:19 PM WIB
#Modified Date: 
#Author: Agung Satria
# START #
sudo add-apt-repository -y ppa:regolith-linux/stable
sudo apt install i3-gaps
echo "Done"
# END #
