#!/bin/bash

function settings {
    options="Adjust Date and Time\nScreenshot Launcher\nScreen Recorder ( +Shift+v)\nOBS Virtual Camera Utility\nQuick Note ( +Shift+n)\nChange Wallpaper\nCalculator ( +q)\nStopwatch\nShow All Keysheets\nVIM Keysheets\nLAMP Control Panel\nCancel"
    selected=$(echo -e $options | dmenu)
    if [[ $selected = "Adjust Date and Time" ]]; then
        st sudo ~/bin/settings/setdate.sh 
    elif [[ $selected = "Screenshot Launcher" ]]; then
        flameshot launcher
    elif [[ $selected = "Screen Recorder ( +Shift+v)" ]]; then
        vokoscreen 
    elif [[ $selected = "OBS Virtual Camera Utility" ]]; then
        st sudo modprobe v4l2loopback 
    elif [[ $selected = "Quick Note ( +Shift+n)" ]]; then
        st nvim ~/notes/ 
    elif [[ $selected = "Change Wallpaper" ]]; then
        nitrogen
    elif [[ $selected = "Calculator ( +q)" ]]; then
        st qalc
    elif [[ $selected = "Stopwatch" ]]; then
        stopwatch
    elif [[ $selected = "Show All Keysheets" ]]; then
        # st nvim -R ~/bin/settings/master-shortcut.txt 
        zathura ~/notes/MASTER-KEYSHEETS.pdf
    elif [[ $selected = "VIM Keysheets" ]]; then
        # st nvim -R ~/bin/settings/master-shortcut.txt 
        zathura ~/notes/vim\ cheat\ sheet.pdf
    elif [[ $selected = "LAMP Control Panel" ]]; then
        /home/agung/bin/settings/lamp.sh
    elif [[ $selected = "Cancel" ]]; then
        return
    fi
}
settings
