#!/bin/bash

function powermenu {
#    options=" Cancel\n Shutdown\n Restart\n Logout\n Sleep\n Lock\n Screenshot\n ScreenRecord"
    options=" Cancel\n Shutdown\n Restart\n Sleep\n Lock\n Logout = Mod+Escape"
    selected=$(echo -e $options | dmenu)
    # 
    if [[ $selected = " Shutdown" ]]; then
        # poweroff
        ~/.config/i3/bin/powermenu/shutdown-confirm.sh
    elif [[ $selected = " Restart" ]]; then
        # reboot
        ~/.config/i3/bin/powermenu/restart-confirm.sh
    elif [[ $selected = " Logout = Mod+Escape" ]]; then
        # killall dwm
        ~/.config/i3/bin/powermenu/logout-confirm.sh
        # return 
    elif [[ $selected = " Sleep" ]]; then
        systemctl suspend
    elif [[ $selected = " Lock" ]]; then
        betterlockscreen --lock
#    elif [[ $selected = " Screenshot" ]]; then
#        flameshot gui
#    elif [[ $selected = " ScreenRecord" ]]; then
#        vokoscreen
    elif [[ $selected = " Cancel" ]]; then
        return
    fi
}
powermenu
