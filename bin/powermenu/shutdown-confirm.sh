#!/bin/bash

function shutdownconfirm {
    options=" Yes, Shutdown\n Cancel"
    selected=$(echo -e $options | dmenu)
    # 
    if [[ $selected = " Yes, Shutdown" ]]; then
        poweroff
    elif [[ $selected = " Cancel" ]]; then
        return
        # ~/.config/i3/bin/powermenu/powermenu.sh
    fi
}
shutdownconfirm
