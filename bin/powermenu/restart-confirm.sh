#!/bin/bash

function restartconfirm {
    options=" Yes, Restart\n Cancel"
    selected=$(echo -e $options | dmenu)
    # 
    if [[ $selected = " Yes, Restart" ]]; then
        reboot
    elif [[ $selected = " Cancel" ]]; then
        return
        # ~/.config/i3/bin/powermenu/powermenu.sh
    fi
}
restartconfirm
