#!/bin/bash
#Author:  Agung Satria

function lamp {
#    options=" Cancel\n Shutdown\n Restart\n Logout\n Sleep\n Lock\n Screenshot\n ScreenRecord"
    # options=" Cancel\n Shutdown\n Restart\n Sleep\n Lock\n Logout = Mod+Ctrl+Q"
    options="Start Apache\nStop Apache\nStart MySql\nStop MySql\nCancel"
    selected=$(echo -e $options | dmenu)
    # 
    if [[ $selected = "Start Apache" ]]; then
        st sudo systemctl start apache2.service
    elif [[ $selected = "Stop Apache" ]]; then
        st sudo systemctl stop apache2.service 
    elif [[ $selected = "Start MySql" ]]; then
        # st sudo systemctl start mariadb.service
        st sudo systemctl start mysql.service
    elif [[ $selected = "Stop MySql" ]]; then
        # st sudo systemctl stop mariadb.service
        st sudo systemctl stop mysql.service
    elif [[ $selected = "Cancel" ]]; then
        # return
        /home/agung/bin/settings/settings.sh
    fi
}
lamp
